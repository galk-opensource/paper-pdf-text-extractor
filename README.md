# Paper Pdf Text Extractor

The repo contains code allowing a user to extract text from academic papers in a PDF format. The text is extracted using several techniques.
Currently, it support the following geometrical and format elements popular in academic papers:

1. Two-column texts: by splitting the page into 2 pages, one per coloum.
2. Graphs, tables and images: finding the boundary of each in a page, and removing it by overriding with a white rectangle.
3. Equations: the OCR providing short and non-words from equations, which are mostly cleared in the post-processing stage.

The result is later saved to a text file (.txt) while allowing to either delete or keep the original .pdf file.

## Background

This repo is a part of a bigger project asking to extract instances of medical nanorobotics from academic papers.  See [Rivendell](http://rivendell.cs.biu.ac.il/).

## Usage 

Install:

1. clone the repo to your local machine
2. load the project to your favorite IDE using Maven
3. simply compile Maven project with all the dependencies in the pom.xml file
4. release the project from your IDE
5. run it using the CLI

Run:

0. make sure you have a folder with academic papers in .pdf format 
1. using the main.java enter the arguments needed for the system according to the prints
2. you can find your results at the path provided to the "out = " field


## Performance

Tested with six papers. Two with two-column format, two with a lot of graphs and images, and two with a lot of equations.
Each paper was manually converted from .pdf to .txt and then converted using the system.  Then both the results were compared using Levenshtein's distance algorithm.

The results: 98.6% of the text was accurate on average. 

package utils.file_handler.txt_handler;

import utils.Logger.LoggerConsole;
import utils.file_handler.FileHandler;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

/*
 The interface to read and write txt files
 */
public class TxtHandler
{
    // consts //
    private static String END_TXT_FILE = ".txt";
    // end - consts //

    /**
     * read a text file as Json string and clear spaces to standatrd
     * @param path - the file path
     * @return the file text
     */
    public static String readJsonText(String path)
    {
        return TxtHandler.readAllText(path).trim().replace("\t", "").replace("\n", "")
                .replace(" \"", "\"").replace(" [", "[").replace(" {", "{")
                .replace("] ", "]").replace(" ,", ",");
    }

    /**
     * read from a txt file all the data inside it
     * @param path - the file path
     * @return the file text
     */
    public static String readAllText(String path)
    {
        String text = "";
        String line = null;
        try
        {
            FileReader fileReader = new FileReader(path);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            while((line = bufferedReader.readLine()) != null)
            {
                text += line;
            }
            bufferedReader.close();
        }
        catch(Exception ex)
        {
            new LoggerConsole("Error in loadBlackList at TextToVec with error: " + ex.getMessage());
            return null;
        }
        return text;
    }

    public static void writeFile(String path, String text)
    {
        writeFile(path, text, true);
    }

    public static void writeFile(String path, List<String> text)
    {
        writeFile(path, text, "\n", false);
    }

    public static void writeFile(String path, List<String> text, String spliter, boolean append)
    {
        StringBuilder finalText = new StringBuilder();
        for (int i = 0; i < text.size() - 1; i++)
        {
            finalText.append(text.get(i)).append(spliter);
        }
        finalText.append(text.get(text.size()-1));
        writeFile(path, finalText.toString(), append);
    }

    /**
     * write a txt file
     * @param path - file path to write
     * @param text - what to write
     * @param append - append or from beggining
     */
    public static void writeFile(String path, String text, boolean append)
    {
        try
        {
            FileWriter writer = new FileWriter(path);
            // append or new
            if (append)
            {
                writer.append(text);
            }
            else
            {
                writer.write(text);
            }
        }
        catch (IOException ex)
        {
            new LoggerConsole("Error in writeFile at TxtHandler with error: " + ex.getMessage());
        }
    }


    /**
     * delete txt file from some path
     * @param pdfPath - path to file wanted to delete
     * @return if deleted or not
     */
    public static boolean deleteFile(String pdfPath)
    {
        // if pdf file
        if(pdfPath.endsWith(END_TXT_FILE))
        {
            return FileHandler.deleteFile(pdfPath);
        }
        return false; // tell not deleted
    }

    /**
     * give just the file name
     * @param path
     * @return
     */
    public static String getName(String path)
    {
        String[] pathElements = path.split("/");
        return pathElements[pathElements.length-1];
    }
}

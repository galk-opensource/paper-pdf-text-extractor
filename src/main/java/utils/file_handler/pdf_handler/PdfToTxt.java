package utils.file_handler.pdf_handler;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.eclipse.jetty.util.IO;
import utils.Logger.LoggerJava;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import utils.images.BoundBox;
import utils.images.TableGraphFinder;
import utils.images.TwoColFinder;

/*
    This class manage all the action we need to convert a pdf to a normal txt file
    handling the following problems:
    1. 2 col pages
    2. graphs and images ignoring
    3. table splitting
 */
public class PdfToTxt
{
    // Logger //
    public static LoggerJava logger = new LoggerJava(PdfToTxt.class.getName());
    // end - Logger //

    private static String FILE_IMAGE_TYPE = "jpeg";
    // end - consts //

    // members //

    // end - members //

    // constractor
    private PdfToTxt()
    {

    }

    //////////////////////
    // filter functions //
    //////////////////////

    // input: pdf file path input
    // output: list of pdf files in order to read
    // action: run all the PDF academic papers and return the order per page with fixed design
    public static List<BufferedImage> handle_problems_in_academic_papers(String pdfPath) throws Exception
    {
        // String savePath = OUT_PATH + new File(pdfPath).getName();
        List<BufferedImage> pageImages = PdfToTxt.pdfToJpg(pdfPath);

        // for each page image
        List<BufferedImage> answer = new ArrayList<>();
        for (BufferedImage pageImage: pageImages)
        {
            // run each algorithm
            pageImage = TableGraphFinder.findAndClearTableGraph(pageImage);
            List<BufferedImage> twoAnswers = twoColoums(pageImage);
            answer.addAll(twoAnswers);
        }
        return answer;
    }

    //TODO: fix this part
    private static List<BufferedImage> twoColoums(BufferedImage pageImage)
    {
        // init answer
        List<BufferedImage> answer = new ArrayList<>();
        // init answer to not find 2 col
        int lineX = TwoColFinder.NO_LINE_FOUND_VALUE;
        try
        {
            // search for 2 col
            lineX = TwoColFinder.findColDivider(pageImage);
        }
        catch (Exception error)
        {
            logger.addLineToLog(logger.WARNING, "IO error at twoColoums at PdfToTxt - " + error.getMessage());
        }

        // check if we don't found 2 col
        if (lineX == TwoColFinder.NO_LINE_FOUND_VALUE)
        {
            // just for standrlization add same image to list and return it
            answer.add(pageImage);
        }
        else
        {
            // get image dims
            int width  = pageImage.getWidth();
            int height = pageImage.getHeight();
            // crop to 2 images and add them to answer
            answer.add(TwoColFinder.cropImage(pageImage, new BoundBox(0, lineX, 0, height)));
            answer.add(TwoColFinder.cropImage(pageImage, new BoundBox(lineX, width, 0, height)));
        }
        return answer;
    }

    ////////////////////////////
    // end - filter functions //
    ////////////////////////////

    /////////////////////
    // image functions //
    /////////////////////
    /**
     * Convert a pdf file to images (one per page)
     * @param pdfFilePath - the pdf file we wish to read
     * @return none
     */
    private static List<BufferedImage> pdfToJpg(String pdfFilePath) throws IOException
    {
        // consts
        final int DPI = 300;
        // init parsers
        PDDocument document = PDDocument.load(new File(pdfFilePath));
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        // init answer
        List<BufferedImage> answer = new ArrayList<>();
        // run on each page
        int pagesInDocument = document.getNumberOfPages();
        for (int page = 0; page < pagesInDocument; ++page)
        {
            answer.add(pdfRenderer.renderImageWithDPI(page, DPI, ImageType.RGB));
        }
        // close document to free the fil
        document.close();
        return answer;
    }

    ///////////////////////////
    // end - image functions //
    ///////////////////////////

}

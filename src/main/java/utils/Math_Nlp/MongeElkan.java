package utils.Math_Nlp;

import java.util.List;

/*
    Manage the distance between 2 abstracts as Monge Elkan
 */
public class MongeElkan
{
    private MongeElkan()
    {

    }

    /**
     * calc the monge elkan distance between 2 arrays
     * @return
     */
    public static double calc(List<String> a, List<String> b) throws Exception {
        if (a != null && b != null && a.size() != 0 && b.size() != 0)
        {
            double answer = 0;
            for (int i = 0; i < a.size(); i++)
            {
                double simMax = 0;
                for (int j = 0; j < b.size(); j++)
                {
                    double score = wordSimilerity(a.get(i), b.get(j)) / Math.max(a.get(i).length(), b.get(j).length()); // score menormal
                    if (score > simMax)
                    {
                        simMax = score;
                    }
                }
                answer += simMax;
            }
            return answer / a.size();
        }
        throw new Exception("can not work on this vectors - cosine");
    }

    // taken from here:  https://en.wikipedia.org/wiki/Levenshtein_distance
    /// <summary>
    ///  Levenshtein Distance algorithm
    /// </summary>
    /// <param name="s"> source string </param>
    /// <param name="t"> target string </param>
    /// <returns> the similarity between them </returns>
    private static double wordSimilerity(String s, String t)
    {
        int n = s.length();
        int m = t.length();
        int[][] d = new int[n + 1][m + 1];

        // Step 1
        if (n == 0)
        {
            return m;
        }

        if (m == 0)
        {
            return n;
        }

        // Step 2
        for (int i = 0; i <= n; d[i][0] = i++)
        {
        }

        for (int j = 0; j <= m; d[0][j] = j++)
        {
        }

        // Step 3
        for (int i = 1; i <= n; i++)
        {
            //Step 4
            for (int j = 1; j <= m; j++)
            {
                // Step 5
                int cost = (t.charAt(j - 1) == s.charAt(i - 1)) ? 0 : 1;

                // Step 6
                d[i][j] = Math.min(
                    Math.min(d[i - 1][j] + 1, d[i][ j - 1] + 1),
                d[i - 1][ j - 1] + cost);
            }
        }
        // Step 7
        return d[n][m];
    }

    // end - help functions //
}

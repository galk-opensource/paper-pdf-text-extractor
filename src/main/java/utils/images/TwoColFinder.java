package utils.images;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.awt.image.BufferedImage;

import static org.opencv.core.Core.bitwise_and;
import static org.opencv.imgproc.Imgproc.*;
import static org.opencv.imgproc.Imgproc.contourArea;

/*
    A class to find in 2 col images of documents the center
 */
public class TwoColFinder {

    // consts //
    // image size
    public static int IMAGE_SIZE_WIDTH = TableGraphFinder.IMAGE_SIZE_WIDTH;
    public static int IMAGE_SIZE_HIEGTH = TableGraphFinder.IMAGE_SIZE_HIEGTH;
    // errors tolerance
    public static double CENTER_IMAGE_COL_ERROR = 0.1;
    // error val
    public static int NO_LINE_FOUND_VALUE = -1;
    // end - consts //

    // constractor
    private TwoColFinder()
    {

    }

    /**
     * find in an image if there is 2 col and if so, where the line goes
     * @param image - the image to work on
     * @return the X value to split the page in
     */
    public static Integer findColDivider(BufferedImage image) throws Exception
    {
        try
        {
            // CONSTS
            final int CHANNEL_SIZE = 3;
            Mat src = TableGraphFinder.bufferedImageToMat(image);
            if (src != null)
            {
                throw new Exception("Error at findColDivider from TwoColFinder - Need image to find things in");
            }

            Mat rsz = new Mat();
            Size size = new Size(IMAGE_SIZE_WIDTH, IMAGE_SIZE_HIEGTH);
            resize(src, rsz, size);

            Mat gray = new Mat();
            if (rsz.channels() == CHANNEL_SIZE)
            {
                cvtColor(rsz, gray, Imgproc.COLOR_BGR2GRAY);
            }
            else {
                gray = rsz; // if single channel then it's already gray
            }

            //Apply adaptive Threshold at the bitwise_not of gray, notice the ~ symbol
            Mat bw = new Mat();
            adaptiveThreshold(gray, bw, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 15, -2);

            // Create the images that will use to extract the vertical lines
            Mat vertical = bw.clone();

            int scale = 3; // play with this variable in order to increase/decrease the amount of lines to be detected
            // Specify size on vertical axis
            int verticalsize = vertical.rows() / scale;

            // Create structure element for extracting vertical lines through morphology operations
            Mat verticalStructure = getStructuringElement(MORPH_RECT, new Size( 1, verticalsize));

            // Apply morphology operations
            erode(vertical, vertical, verticalStructure);
            dilate(vertical, vertical, verticalStructure); // expand vertical lines

            // TODO: finish here
            /*
            bestLine = null;
            int lineX = getXfromLine(bestLine);
            if (lineX > IMAGE_SIZE_WIDTH * (1 - CENTER_IMAGE_COL_ERROR) && lineX < IMAGE_SIZE_WIDTH * (1 - CENTER_IMAGE_COL_ERROR))
            {
                return lineX;
            }
            */
            return NO_LINE_FOUND_VALUE;
        }
        catch (Exception error)
        {
            throw new Exception("Error at findColDivider from TwoColFinder - " + error.getMessage());
        }
    }

    ////////////////////
    // help functions //
    ////////////////////

    /**
     * crop image to sub image according to some bb
     * @param src - the image to crop
     * @param box - the bbox to crop according to
     * @return the croped image
     */
    public static BufferedImage  cropImage(BufferedImage src, BoundBox box)
    {
        try {
            return src.getSubimage(box.x_min, box.x_max, (int)box.width(), (int)box.height());
        }
        catch (Exception error)
        {
            // error value
            return null;
        }

    }

    //////////////////////////
    // end - help functions //
    //////////////////////////
}
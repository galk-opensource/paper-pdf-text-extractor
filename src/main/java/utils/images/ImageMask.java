package utils.images;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import utils.images.BoundBox;

/*
    This class allows to change an image by drawing on it
 */
public class ImageMask
{
    // shadow constractor
    private ImageMask()
    {

    }

    public static BufferedImage drawRect(String imagePath, BoundBox bb) throws IOException
    {
        return ImageMask.drawRect(ImageIO.read(new File(imagePath)), bb.x_min, bb.x_max, bb.y_min, bb.y_max);
    }

    public static BufferedImage drawRect(BufferedImage image, BoundBox bb)
    {
        return ImageMask.drawRect(image, bb.x_min, bb.x_max, bb.y_min, bb.y_max);
    }

    public static BufferedImage drawRect(String imagePath, int x_min, int x_max, int y_min, int y_max) throws IOException
    {
        return ImageMask.drawRect(ImageIO.read(new File(imagePath)), x_min, x_max, y_min, y_max);
    }

    public static BufferedImage drawRect(BufferedImage image, int x_min, int x_max, int y_min, int y_max)
    {
        Graphics2D g2d = image.createGraphics();
        g2d.setColor(Color.WHITE);
        g2d.fillRect(x_min, y_min, x_max - x_min, y_max - y_min);
        g2d.dispose();
        return image;
    }
}

package utils.images;

import utils.Pair;

/*
    A class to hold a bounding box in an image
 */
public class BoundBox {

    // members //
    public int x_min;
    public int x_max;
    public int y_min;
    public int y_max;
    // end - members //

    public BoundBox(int x_min, int x_max, int y_min, int y_max)
    {
        this.x_min = x_min;
        this.x_max = x_max;
        this.y_min = y_min;
        this.y_max = y_max;
    }

    ////////////////////
    // calc functions //
    ////////////////////

    public double width()
    {
        return x_max - x_min;
    }

    public double height()
    {
        return y_max - y_min;
    }

    public Pair<Double, Double> center()
    {
        return new Pair<>(width()/2, height()/2);
    }

    public Double area()
    {
        return width() * height();
    }

    //////////////////////////
    // end - calc functions //
    //////////////////////////
}

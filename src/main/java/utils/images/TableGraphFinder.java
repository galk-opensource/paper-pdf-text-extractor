package utils.images;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import utils.images.ImageMask;
import utils.images.BoundBox;

import static org.opencv.core.Core.bitwise_and;
import static org.opencv.imgproc.Imgproc.*;

/*
    Find table or graph in a document image
 */
public class TableGraphFinder {

    // consts //
    public static int IMAGE_SIZE_WIDTH = 800;
    public static int IMAGE_SIZE_HIEGTH = 900;

    public static int MIN_TABLE_AREA = 100;
    // end - consts //

    // constractor
    private TableGraphFinder()
    {

    }

    /**
     * find and mask tables and graphs and images from document
     * @param image - the image to mask
     * @return the same image without the tables and images and graphs
     */
    public static BufferedImage findAndClearTableGraph(BufferedImage image) throws Exception
    {
        try {
            List<BoundBox> bboxs = findTableGraph(image);
            // mask each bound box
            for (BoundBox bbox: bboxs)
            {
                ImageMask.drawRect(image, bbox);
            }
            return image;
        }
        catch (Exception error)
        {
            throw new Exception("Error at findAndClearTableGraph from TableGraphFinder - " + error.getMessage());
        }
    }

    /**
     * Find tables and graphs by lines using hough transform and canny
     * @param image- the image we working on
     * @return the bound boxes of such instances
     */
    public static List<BoundBox> findTableGraph(BufferedImage image) throws Exception
    {
        // Using : http://answers.opencv.org/question/63847/how-to-extract-tables-from-an-image/
        // need to change to: https://blog.goodaudience.com/table-detection-using-deep-learning-7182918d778?fbclid=IwAR3jHzgq63fBOQWPfx0xTv3cJ1oAdcSzeYacH9PBG2TIMbQM6H53TGFiIa

        // CONSTS
        final int CHANNEL_SIZE = 3;
        Mat src = bufferedImageToMat(image);
        if (src != null)
        {
            throw new Exception("Error at findTableGraph from TableGraphFinder - Need image to find things in");
        }

        Mat rsz = new Mat();
        Size size = new Size(IMAGE_SIZE_WIDTH, IMAGE_SIZE_HIEGTH);
        resize(src, rsz, size);

        Mat gray = new Mat();
        if (rsz.channels() == CHANNEL_SIZE)
        {
            cvtColor(rsz, gray, Imgproc.COLOR_BGR2GRAY);
        }
        else {
            gray = rsz; // if single channel then it's already gray
        }

        //Apply adaptive Threshold at the bitwise_not of gray, notice the ~ symbol
        Mat bw = new Mat();
        adaptiveThreshold(gray, bw, 255, ADAPTIVE_THRESH_GAUSSIAN_C, THRESH_BINARY, 15, -2);

        // Create the images that will use to extract the horizontal and vertical lines
        Mat horizontal = bw.clone();
        Mat vertical = bw.clone();

        int scale = 15; // play with this variable in order to increase/decrease the amount of lines to be detected

        // Specify size on horizontal axis
        int horizontalsize = horizontal.cols() / scale;

        // Create structure element for extracting horizontal lines through morphology operations
        Mat horizontalStructure = getStructuringElement(MORPH_RECT, new Size(horizontalsize,1));

        // Apply morphology operations
        erode(horizontal, horizontal, horizontalStructure);
        dilate(horizontal, horizontal, horizontalStructure);  // expand horizontal lines

        // Specify size on vertical axis
        int verticalsize = vertical.rows() / scale;

        // Create structure element for extracting vertical lines through morphology operations
        Mat verticalStructure = getStructuringElement(MORPH_RECT, new Size( 1, verticalsize));

        // Apply morphology operations
        erode(vertical, vertical, verticalStructure);
        dilate(vertical, vertical, verticalStructure); // expand vertical lines

        // create a mask which includes the tables
        Mat mask = new Mat();
        Core.add(horizontal, vertical, mask);

        // find the joints between the lines of the tables, we will use this information in order to descriminate tables from pictures (tables will contain more than 4 joints while a picture only 4 (i.e. at the corners))
        Mat joints = new Mat();
        bitwise_and(horizontal, vertical, joints);

        // Find external contours from the mask, which most probably will belong to tables or to images
        Mat hierarchy = new Mat();
        List<MatOfPoint> contours = new ArrayList<>();
        findContours(mask, contours, hierarchy, RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));

        List<MatOfPoint> contours_poly = new ArrayList<>(contours.size());
        List<Rect> boundRect = new ArrayList<>(contours.size());
        List<Mat> rois = new ArrayList<>();

        for (int i = 0; i < contours.size(); i++)
        {
            // find the area of each contour
            double area = contourArea(contours.get(i));
            // filter individual lines of blobs that might exist and they do not represent a table
            if (area < MIN_TABLE_AREA)
            {
                continue;
            }
        }
        return null;
    }

    ///////////////////////////////
    // help functions for search //
    ///////////////////////////////

    /**
     * Convert BufferedImage to Mat for openCV work
     * @param bi - the image we need to convert
     * @return the image as Mat for openCV
     */
    public static Mat bufferedImageToMat(BufferedImage bi) {
        Mat mat = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC3);
        byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
        mat.put(0, 0, data);
        return mat;
    }

    /////////////////////////////////////
    // end - help functions for search //
    /////////////////////////////////////
}

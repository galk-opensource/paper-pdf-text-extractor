package PaperPdfTextExtractor;

import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/*
This class manage the operation of all the library
from PDF data to a Property-Value table
 */
public class LibraryManager
{
    /**
     * Convert the .pdf files we downloaded
     * to .txt file so we will be able to work this it
     * @param basicPull - if basic pull or using ocr and vision
     * @param needDeleteFile - if we need to delete the pdf files
     */
    private List<String> convertPdfsToText(Boolean basicPull, Boolean needDeleteFile)
    {
        final String END_TXT = ".txt";
        List<String> errorFiles = new ArrayList<>();
        int countFilesReviewed = 0;

        int countFiles = 0;
        int countPassFiles = 0;

        // try to get to the folder with all the pdfs we downloaded
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(this.articlesFolderPath)))
        {
            for (Path p : stream)
            {
                try
                {
                    // make sure we work only of pdf files
                    if (!p.toString().endsWith(PDF_TYPE))
                    {
                        continue;
                    }

                    String txtFilePath = this.articlesFolderPath + "/" + p.toFile().getName().toLowerCase() + END_TXT;
                    // convert pdf file into txt file
                    String pdfStringToWrite;
                    countFilesReviewed++;
                    if (basicPull) // basic or advanced pull
                    {
                        pdfStringToWrite = PdfHandler.getPdfTextClear(p.toString());
                    }
                    else
                    {
                        try
                        {
                            pdfStringToWrite = PdfHandler.getAcademicClearPdf(p.toString());
                        }
                        catch (Exception error)
                        {
                            logger.addLineToLog(logger.WARNING, "Error at convertPdfsToText from LibraryManager with sub error " + error.getMessage());
                            // skip this file
                            continue;
                        }
                    }
                    TxtHandler.writeFile(txtFilePath, pdfStringToWrite);
                    logger.addLineToLog("file " + p.toString() + " converted to .txt file in: " + txtFilePath);
                    // delete the pdf cause we don't need it anymore
                    if(needDeleteFile && !PdfHandler.deleteFile(p.toString()))
                    {
                        // let know that  we have some problem
                        logger.addLineToLog("Could not delete pdf file in paht: " + p.toString());
                    }
                }
                catch (Exception ex)
                {
                    logger.addLineToLog("Error at countFilesInFolder in SystemTest with: "
                            + ex.getMessage() + " when reviewing " + p.toFile().getName());
                    errorFiles.add(p.toFile().getName());
                }
            }
            logger.addLineToLog("Reviewed " + countFilesReviewed + " articles, from there " + errorFiles.size() + " not converted");
            for (String errorFilePath: errorFiles)
            {
                logger.addLineToLog("Error article to convert at " + errorFilePath);
            }
        }
        catch (IOException ex)
        {
            logger.addLineToLog("Error at countFilesInFolder in SystemTest with: " + ex.getMessage());
        }
        // return error files
        return errorFiles;
    }